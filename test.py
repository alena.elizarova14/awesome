# -*- coding: utf-8 -*
import os
import sys
import re
from subprocess import call

#не забыть про конфиг файл проекта - добавить туда поле git_repo, если нет
#глоссарий
#по завершению удалить все промежуточные файлы и директории

def recursive_include(include_file_name):
	include_file(include_file_name)

include_pattern = re.compile('{{(.+)}}') #include generic pattern

def process_file(main_file_name):
	str_list = []
	f1 = open(main_file_name, 'r')
	#new = open('output_new.md', 'w')
	f = f1.readlines()
	for line in f:
		found = include_pattern.search(line)
		if found:
			include_file_name = found.group(1)
			change_needed = found.group(0)
			if '.md' not in include_file_name:
				include_file_name = include_file_name + '.md'
			walk_dir = os.getcwd()
			for root, subdirs, files in os.walk(walk_dir):
				include_file_test = os.path.join(root, include_file_name)
				if os.path.isfile(include_file_test):
					#inc = open(include_file_test, 'r')
					#included_content = inc.read()
					str = process_file(include_file_test)
					str_list.extend(str)
		else:
			str_list.append(line)
			#line = line.replace(change_needed, included_content) #заменить на re.sub
		#new.write(line) 
	#f1.close()
	#new.close()
	#os.rename('output_new.md', 'output.md
	return str_list
    
def write_lines_to_file(str_list):
	new_md = open('output_new.md', 'w')
	for line in str_list:
		new_md.write(str(line))
	new_md.close()


def pdf_from_md(file_name):
	call(['pandoc', file_name, '-s', '-o', 'output.pdf', '--latex-engine=xelatex'])
def remove_staging_files():
	os.remove('output_new.md')

str_list =  process_file('output.md')
write_lines_to_file(str_list)
#pdf_from_md('output_new.md')
#remove_staging_files()


#{{((git){0,1}(?=(:))){0,1}(.+)((?::)(.*))(?:-)(.*)}} - усложненная по найденному выражению в фигурных скобках

#{{((git){0,1}(?=(:))){0,1}(.+)(?::)(.+)(?:-)(.*)}} 
#можно расплитить по : 1-ая группа гит, 2 - путь, 3 - 

#{{((?P<git_group>git)(?::))?(?P<path_group>.+)?(?::)(?P<start_head>.+)?(?:-)(?P<end_head>.+)}} -  не работают 
#test string {{git:some_more_doc:heading1-heading2}}